// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/OCBoardFENComponent.h"

#include "Pieces/OCPieceBase.h"

UOCBoardFENComponent::UOCBoardFENComponent()
{
	for (EPieceType PieceType : TEnumRange<EPieceType>())
	{
		PieceTypeClasses.Add(PieceType, nullptr);
	}

	StartingPiecesPosition = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR";
}

TMultiMap<TSubclassOf<AOCPieceBase>, EPieceColor> UOCBoardFENComponent::GetPiecePosition()
{
	TMultiMap<TSubclassOf<AOCPieceBase>, EPieceColor> PiecePosition;
	for (const char PieceChar : StartingPiecesPosition)
	{
		if (PieceChar == '/')
		{
			continue;
		}

		if (PieceChar == '1')
		{
			PiecePosition.Add( nullptr, EPieceColor::Unknown );
			continue;
		}
		
		if (PieceChar == '8')
		{
			for (int32 i = 0; i < static_cast<int32>(PieceChar) - 48; ++i)
			{
				PiecePosition.Add( nullptr, EPieceColor::Unknown );
			}
			continue;
		}
		
		PiecePosition.Add( GetFilteredPieceClass(PieceChar), GetFilteredPieceColor(PieceChar) );
	}
	return PiecePosition;
}

TSubclassOf<AOCPieceBase> UOCBoardFENComponent::GetFilteredPieceClass(char PieceChar) const
{
	const char LowerPieceChar = std::tolower(PieceChar);
	switch (LowerPieceChar)
	{
	case 'p':
		return PieceTypeClasses[EPieceType::Pawn];
	case 'b':
		return PieceTypeClasses[EPieceType::Bishop];
	case 'n':
		return PieceTypeClasses[EPieceType::Knight];
	case 'r':
		return PieceTypeClasses[EPieceType::Rook];
	case 'q':
		return PieceTypeClasses[EPieceType::Queen];
	case 'k':
		return PieceTypeClasses[EPieceType::King];
	default:
		return nullptr;
	}
}

EPieceColor UOCBoardFENComponent::GetFilteredPieceColor(char PieceChar) const
{
	if (std::isupper(PieceChar))
	{
		return EPieceColor::White;
	}
	return EPieceColor::Black;
}

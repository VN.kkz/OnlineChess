// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/OCPieceMovementComponent.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "Pieces/OCPieceBase.h"
#include "Board/OCBoard.h"
#include "Board/OCBoardTile.h"

void UOCPieceMovementComponent::GetPossibleMoves(TArray<AOCBoardTile*>& Moves, EPieceMoveDirection MoveDirection,
	bool bCheckForAttackMoves) const
{
	const AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
	const int32 NumSquares = Board->GetNumSquares();
	for (int32 MoveDistanceValue = 1; MoveDistanceValue < NumSquares; ++MoveDistanceValue)
	{
		const FOCBoardCoordinates PossibleMoveCoordinates = GetPossibleMoveCoordinates(MoveDirection, MoveDistanceValue);
		if (PossibleMoveCoordinates.Rank == -1 ||
			PossibleMoveCoordinates.File == -1)
		{
			continue;
		}
		
		const bool bMoveWasBlocked = AddPossibleMove(Moves, bCheckForAttackMoves, PossibleMoveCoordinates);
		if (bMoveWasBlocked)
		{
			break;
		}
	}
}

TPair<int32, int32> UOCPieceMovementComponent::GetDirectionMultiplier(EPieceMoveDirection MoveDirection) const
{
	switch(MoveDirection)
	{
	case EPieceMoveDirection::Top:
	case EPieceMoveDirection::Right:
		return TPair<int32, int32>(1, 1);
	case EPieceMoveDirection::Bottom:
	case EPieceMoveDirection::Left:
		return TPair<int32, int32>(-1, -1);
	case EPieceMoveDirection::TopLeft:
		return TPair<int32, int32>(1, -1);
	case EPieceMoveDirection::TopRight:
		return TPair<int32, int32>(1, 1);
	case EPieceMoveDirection::BottomLeft:
		return TPair<int32, int32>(-1, -1);
	case EPieceMoveDirection::BottomRight:
		return TPair<int32, int32>(-1, 1);
	default:
		return TPair<int32, int32>(0, 0);
	}
}

FOCBoardCoordinates UOCPieceMovementComponent::GetPossibleMoveCoordinates(EPieceMoveDirection MoveDirection,
                                                                          int32 MoveDistanceValue) const
{
	FOCBoardCoordinates PossibleMoveCoordinates;
	
	if (MoveDirection == EPieceMoveDirection::Top || MoveDirection == EPieceMoveDirection::Bottom)
	{
		PossibleMoveCoordinates.Rank = GetOwnerCoordinates().Rank + (MoveDistanceValue * GetDirectionMultiplier(MoveDirection).Key);
		PossibleMoveCoordinates.File = GetOwnerCoordinates().File;
	}
	else if (MoveDirection == EPieceMoveDirection::Left || MoveDirection == EPieceMoveDirection::Right)
	{
		PossibleMoveCoordinates.Rank = GetOwnerCoordinates().Rank;
		PossibleMoveCoordinates.File = GetOwnerCoordinates().File + (MoveDistanceValue * GetDirectionMultiplier(MoveDirection).Key);
	}
	else
	{
		PossibleMoveCoordinates.Rank = GetOwnerCoordinates().Rank + (MoveDistanceValue * GetDirectionMultiplier(MoveDirection).Key);
		PossibleMoveCoordinates.File = GetOwnerCoordinates().File + (MoveDistanceValue * GetDirectionMultiplier(MoveDirection).Value);
	}
	
	const AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
	const int32 NumSquares = Board->GetNumSquares();
	if (PossibleMoveCoordinates.Rank != FMath::Clamp(PossibleMoveCoordinates.Rank, 0, NumSquares - 1) ||
		PossibleMoveCoordinates.File != FMath::Clamp(PossibleMoveCoordinates.File, 0, NumSquares - 1))
	{
		return FOCBoardCoordinates(-1, -1);
	}
	
	return PossibleMoveCoordinates;
}

bool UOCPieceMovementComponent::AddPossibleMove(TArray<AOCBoardTile*>& PossibleMoves, bool bCheckForAttackMoves,
	FOCBoardCoordinates Coordinates) const
{
	const AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
	const int32 NumSquares = Board->GetNumSquares();
	const int32 BoardTileIndex = (NumSquares * Coordinates.Rank) + Coordinates.File;
	
	TArray<AOCBoardTile*> BoardTiles = Board->BoardTiles;
	if (BoardTiles.IsValidIndex(BoardTileIndex))
	{
		AOCBoardTile* BoardTile = BoardTiles[BoardTileIndex];
		if (bCheckForAttackMoves)
		{
			if (IsEnemy(BoardTile->Piece))
			{
				PossibleMoves.Add(BoardTile);
				return true;
			}
			if (BoardTile->Piece != nullptr)
			{
				return true;
			}
		}
		else
		{
			if (BoardTile->Piece == nullptr)
			{
				PossibleMoves.Add(BoardTile);
			}
			else
			{
				return true;
			}
		}
	}

	return false;
}

bool UOCPieceMovementComponent::IsAlly(AOCPieceBase* OtherPiece) const
{
	if (OtherPiece)
	{
		if (OtherPiece->GetColor() == GetOwnerPiece()->GetColor())
		{
			return true;
		}
	}
	return false;
} 

bool UOCPieceMovementComponent::IsEnemy(AOCPieceBase* OtherPiece) const
{
	if (OtherPiece)
	{
		return !IsAlly(OtherPiece);
	}
	return false;
}

AOCPieceBase* UOCPieceMovementComponent::GetOwnerPiece() const
{
	return Cast<AOCPieceBase>(GetOwner());
}

FOCBoardCoordinates UOCPieceMovementComponent::GetOwnerCoordinates() const
{
	FOCBoardCoordinates Coordinates;
	
	const AOCPieceBase* Piece = GetOwnerPiece();
	if (Piece)
	{
		Coordinates = Piece->GetCoordinates(); 
	}

	return Coordinates;
}

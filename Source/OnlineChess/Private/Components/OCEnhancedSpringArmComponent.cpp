// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/OCEnhancedSpringArmComponent.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "GameFramework/SpringArmComponent.h"

UOCEnhancedSpringArmComponent::UOCEnhancedSpringArmComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	
	bDoCollisionTest = false;

	DefaultCameraZoom = 1000.0f;
	MinCameraZoom = 800.0f;
	MaxCameraZoom = 2000.0f;
	CameraZoomSpeed = 200.0f;
	CameraZoomScale = 200.0f;
	CameraZoomDifference = 0.0f;

	bIsCameraRotating = false;
	DefaultCameraRotationDegree = -65.0f;
	MinCameraRotationDegree = -75.0f;
	MaxCameraRotationDegree = -20.0f;
	CameraRotationScale = 3.0f;
	CameraSideRotationScale = 2.0f;
	CameraSideRotationSpeed = 3.0f;
	
	TargetArmLength = DefaultCameraZoom;
	SetWorldRotation(FRotator(DefaultCameraRotationDegree, 0.0f, 0.0f));
}

void UOCEnhancedSpringArmComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateCameraLocation(DeltaTime);
}

void UOCEnhancedSpringArmComponent::BeginPlay()
{
	Super::BeginPlay();

	UpdateScaling();
}

void UOCEnhancedSpringArmComponent::UpdateScaling()
{
	const float BoardScale = UOCGameplayFunctionLibrary::GetBoardScale(this);
	DefaultCameraZoom *= BoardScale;
	MinCameraZoom *= BoardScale;
	MaxCameraZoom *= BoardScale;
	CameraZoomSpeed *= BoardScale;
	CameraZoomScale *= BoardScale;
	TargetArmLength = DefaultCameraZoom;
}

void UOCEnhancedSpringArmComponent::UpdateCameraLocation(float DeltaSeconds)
{
	constexpr float DefaultCameraLocation = 0.0f;
	
	if (CameraZoomDifference > DefaultCameraLocation)
	{
		const float BoardScale = UOCGameplayFunctionLibrary::GetBoardScale(this);
		const float NewTargetArmLength = FMath::FInterpTo(TargetArmLength,
						 CameraZoomDifference, DeltaSeconds * BoardScale, CameraZoomSpeed);
		TargetArmLength = NewTargetArmLength;
	}

	FRotator Rotation = GetComponentRotation();
	if (!bIsCameraRotating && Rotation.Yaw != DefaultCameraLocation)
	{
		const float NewRotation = FMath::FInterpTo(Rotation.Yaw,
			DefaultCameraLocation, DeltaSeconds, CameraSideRotationSpeed);
		Rotation.Yaw = NewRotation;
		SetWorldRotation(Rotation);
	}
}

void UOCEnhancedSpringArmComponent::ZoomCamera(float AxisValue)
{
	CameraZoomDifference = TargetArmLength;
	CameraZoomDifference += AxisValue * -CameraZoomScale;
	CameraZoomDifference = FMath::Clamp(CameraZoomDifference, MinCameraZoom, MaxCameraZoom);
}

void UOCEnhancedSpringArmComponent::LockCameraRotation()
{
	bIsCameraRotating = !bIsCameraRotating;
}

void UOCEnhancedSpringArmComponent::RotateCameraByY(float AxisValue)
{
	if (bIsCameraRotating)
	{
		FRotator NewRotation = GetComponentRotation();
		NewRotation.Pitch += AxisValue / CameraRotationScale;
		if (NewRotation.Pitch > MinCameraRotationDegree && NewRotation.Pitch < MaxCameraRotationDegree)
		{
			SetWorldRotation(NewRotation);
		}
	}
}

void UOCEnhancedSpringArmComponent::RotateCameraByZ(float AxisValue)
{
	if (bIsCameraRotating)
	{
		FRotator NewRotation = GetComponentRotation();
		NewRotation.Yaw += AxisValue / CameraSideRotationScale;
		SetWorldRotation(NewRotation);
	}
}

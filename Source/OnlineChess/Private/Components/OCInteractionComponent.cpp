// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/OCInteractionComponent.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "Board/OCBoard.h"
#include "Kismet/GameplayStatics.h"
#include "Board/OCBoardTile.h"
#include "Pieces/OCPieceBase.h"
#include "Player/OCPlayerPawn.h"

UOCInteractionComponent::UOCInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	
	DraggingPieceOffsetX = 25.0f;
	DraggingPieceOffsetZ = 10.0f;
	InteractionDistance = 2000.0f;
}

void UOCInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdatePieceLocation();
	UpdateBoardTiles();
	UpdateCursorAppearance();
}

void UOCInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	const float BoardScale = UOCGameplayFunctionLibrary::GetBoardScale(this);
	DraggingPieceOffsetX *= BoardScale;
	DraggingPieceOffsetZ *= BoardScale;
	InteractionDistance *= BoardScale;
}

void UOCInteractionComponent::Server_TryToMakeMove_Implementation(AOCPieceBase* Piece, AOCBoardTile* FirstBoardTile,
	AOCBoardTile* SecondBoardTile, bool bForcePositionReset)
{
	UOCGameplayFunctionLibrary::MakeMove(Piece, FirstBoardTile, SecondBoardTile, bForcePositionReset);
}

bool UOCInteractionComponent::LockPiece(AOCPieceBase*& Piece)
{
	if (Piece->GetColor() == GetOwnerPieceColor())
	{
		SelectedBoardTile = GetBoardTile(false);
		if (SelectedBoardTile)
		{
			SetBuffers(Piece);
			
			return true;
		}
	}
	
	return false;
}

void UOCInteractionComponent::UnlockPiece(AOCPieceBase*& Piece, bool bForcePositionReset /* = false */)
{
	Server_TryToMakeMove(Piece, GetBoardTile(true), SelectedBoardTile, bForcePositionReset);
	
	SelectedBoardTile = nullptr;
	Piece = nullptr;
	ClearBuffers();
}

void UOCInteractionComponent::SelectOrPlacePiece()
{
	const FHitResult HitResultPiece = LineTraceBoardTile(false);
	AOCPieceBase* NewSelectedPiece = Cast<AOCPieceBase>(HitResultPiece.GetActor());
	if (NewSelectedPiece && NewSelectedPiece != SelectedPiece && NewSelectedPiece->GetColor() == GetOwnerPieceColor())
	{
		if (LockPiece(NewSelectedPiece))
		{
			SelectedPiece = NewSelectedPiece;
		}
	}
	else
	{
		UnlockPiece(SelectedPiece);
	}
}

void UOCInteractionComponent::HoldPiece()
{
	if (HoldingPiece == nullptr)
	{
		const FHitResult HitResultPiece = LineTraceBoardTile(false);
		HoldingPiece = Cast<AOCPieceBase>(HitResultPiece.GetActor());
		if (HoldingPiece)
		{
			SelectedPiece = nullptr;
			ClearBuffers();
			
			if (!LockPiece(HoldingPiece))
			{
				HoldingPiece = nullptr;
			}
		}
	}
}

void UOCInteractionComponent::PlacePiece()
{
	UnlockPiece(HoldingPiece);
}

void UOCInteractionComponent::DeselectPiece()
{
	UnlockPiece(SelectedPiece, true);
	UnlockPiece(HoldingPiece, true);
}

void UOCInteractionComponent::SetBuffers(AOCPieceBase* Piece)
{
	LegalMovesBuffer = Piece->GetLegalMoves();
	AttackMovesBuffer = Piece->GetAttackMoves();
}

void UOCInteractionComponent::ClearBuffers()
{
	LegalMovesBuffer.Empty();
	AttackMovesBuffer.Empty();
}

APlayerController* UOCInteractionComponent::GetOwnerPlayerController() const
{
	const APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (PawnOwner)
	{
		return Cast<APlayerController>(PawnOwner->Controller);
	}
	return nullptr;
}

EPieceColor UOCInteractionComponent::GetOwnerPieceColor() const
{
	const AOCPlayerPawn* PlayerPawnOwner = Cast<AOCPlayerPawn>(GetOwner());
	if (PlayerPawnOwner)
	{
		return PlayerPawnOwner->GetColor();
	}
	return EPieceColor::Unknown;
}

AOCBoardTile* UOCInteractionComponent::GetBoardTile(bool bIgnoreHoldingAndSelectedPieces) const
{
	AOCBoardTile* BoardTile;
	
	const FHitResult HitResultPiece = LineTraceBoardTile(bIgnoreHoldingAndSelectedPieces);
	const AOCPieceBase* Piece = Cast<AOCPieceBase>(HitResultPiece.GetActor());
	if (Piece)
	{
		FVector TraceLocationStart = Piece->GetActorLocation();
		FVector TraceLocationEnd = TraceLocationStart;
		TraceLocationStart.Z += InteractionDistance;
		TraceLocationEnd.Z += -InteractionDistance;
			
		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.AddIgnoredActor(Piece);

		FHitResult NewHitResultBoardTile;
		GetWorld()->LineTraceSingleByChannel(NewHitResultBoardTile, TraceLocationStart, TraceLocationEnd,
			ECC_GameTraceChannel1, CollisionQueryParams);
			
		BoardTile = Cast<AOCBoardTile>(NewHitResultBoardTile.GetActor());
	}
	else
	{
		BoardTile = Cast<AOCBoardTile>(HitResultPiece.GetActor());
	}

	return BoardTile;
}

FHitResult UOCInteractionComponent::LineTraceBoardTile(bool bIgnoreHoldingAndSelectedPieces) const
{
	FHitResult HitResult;
	APlayerController* PlayerController = GetOwnerPlayerController();
	if (PlayerController)
	{
		FVector2D MousePosition;
		PlayerController->GetMousePosition(MousePosition.X, MousePosition.Y);
			
		FVector WorldOrigin;
		FVector WorldDirection;
		if (UGameplayStatics::DeprojectScreenToWorld(PlayerController, MousePosition, WorldOrigin, WorldDirection) == true)
		{
			FCollisionQueryParams CollisionQueryParams;
			if (bIgnoreHoldingAndSelectedPieces)
			{
				CollisionQueryParams.AddIgnoredActor(SelectedPiece);
				CollisionQueryParams.AddIgnoredActor(HoldingPiece);
			}
				
			GetWorld()->LineTraceSingleByChannel(HitResult, WorldOrigin, WorldOrigin + WorldDirection * InteractionDistance, ECC_GameTraceChannel1, CollisionQueryParams);
		}
	}
	
	return HitResult;
}

void UOCInteractionComponent::UpdatePieceLocation() const
{
	if (HoldingPiece)
	{
		FHitResult HitResult = LineTraceBoardTile(true);
		HitResult.Location.X -= DraggingPieceOffsetX;
		HitResult.Location.Z += DraggingPieceOffsetZ;
		HoldingPiece->SetActorLocation(HitResult.Location);
	}
}

void UOCInteractionComponent::UpdateBoardTiles()
{
	for (AOCBoardTile* BoardTile : UOCGameplayFunctionLibrary::GetBoard(this)->BoardTiles)
	{
		BoardTile->Clear();
	}
	
	for (AOCBoardTile* BoardTile : UOCGameplayFunctionLibrary::GetLastMoves(this))
	{
		BoardTile->SetInnerHighlighted();
	}

	AOCBoardTile* CheckTile = UOCGameplayFunctionLibrary::GetCheckTile(this);
	if (CheckTile)
	{
		CheckTile->SetWarningHighlighted();
	}
	
	if (SelectedPiece || HoldingPiece)
	{
		for (AOCBoardTile* BoardTile : LegalMovesBuffer)
		{
			BoardTile->SetLegalMove();
		}

		for (AOCBoardTile* BoardTile : AttackMovesBuffer)
		{
			BoardTile->SetLegalAttack();
		}
	
		if (SelectedBoardTile)
		{
			SelectedBoardTile->SetHighlighted();
		}
			
		AOCBoardTile* HitBoardTile = GetBoardTile(true);
		if (HitBoardTile && HitBoardTile->IsLegal())
		{
			HitBoardTile->SetHighlighted();
		}
	}
}

void UOCInteractionComponent::UpdateCursorAppearance() const
{
	APlayerController* PlayerController = GetOwnerPlayerController();
	if (PlayerController)
	{
		if (HoldingPiece)
		{
			PlayerController->CurrentMouseCursor = EMouseCursor::GrabHandClosed;
			return;
		}
			
		const FHitResult HitResult = LineTraceBoardTile(false);
		const AOCPieceBase* HitPiece = Cast<AOCPieceBase>(HitResult.GetActor());
		if (HitPiece && HitPiece->GetColor() == GetOwnerPieceColor())
		{
			PlayerController->CurrentMouseCursor = EMouseCursor::GrabHand;
			return;
		}
				
		PlayerController->CurrentMouseCursor = EMouseCursor::Default;
	}
}

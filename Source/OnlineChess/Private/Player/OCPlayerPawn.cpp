// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/OCPlayerPawn.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Core/OCGameplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/OCEnhancedSpringArmComponent.h"
#include "Components/OCInteractionComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Pieces/OCPieceBase.h"
#include "Player/OCInputConfigData.h"
#include "Player/OCPlayerController.h"

AOCPlayerPawn::AOCPlayerPawn()
{
	InteractionComponent = CreateDefaultSubobject<UOCInteractionComponent>(TEXT("InteractionComponent"));

	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	DefaultRootComponent->SetupAttachment(RootComponent);
	
	EnhancedSpringArmComponent = CreateDefaultSubobject<UOCEnhancedSpringArmComponent>(TEXT("EnhancedSpringArmComponent"));
	EnhancedSpringArmComponent->SetupAttachment(DefaultRootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(EnhancedSpringArmComponent);

	StartGameUpdateRate = 0.5f;
	
	Color = EPieceColor::Unknown;
	bCanMovePieces = false;
}

void AOCPlayerPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AOCPlayerPawn, bCanMovePieces);
	DOREPLIFETIME(AOCPlayerPawn, Color);
}

void AOCPlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (PlayerController)
	{
		UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
		if (Subsystem)
		{
			Subsystem->ClearAllMappings();
			Subsystem->AddMappingContext(InputConfigData->InputMappingContext, 0);
		}
	}
}

void AOCPlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	UEnhancedInputComponent* EnhancedInput = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	
	EnhancedInput->BindAction(InputConfigData->SelectPiece, ETriggerEvent::Canceled, this, &AOCPlayerPawn::InputSelectOrPlacePiece);
	EnhancedInput->BindAction(InputConfigData->SelectPiece, ETriggerEvent::Triggered, this, &AOCPlayerPawn::InputHoldPiece);
	EnhancedInput->BindAction(InputConfigData->SelectPiece, ETriggerEvent::Completed, this, &AOCPlayerPawn::InputPlacePiece);
	EnhancedInput->BindAction(InputConfigData->DeselectPiece, ETriggerEvent::Started, this, &AOCPlayerPawn::InputDeselectPiece);
	
	EnhancedInput->BindAction(InputConfigData->ZoomCamera, ETriggerEvent::Triggered, this, &AOCPlayerPawn::InputZoomCamera);
	EnhancedInput->BindAction(InputConfigData->RotateCamera, ETriggerEvent::Started, this, &AOCPlayerPawn::InputLockCameraRotation);
	EnhancedInput->BindAction(InputConfigData->RotateCamera, ETriggerEvent::Completed, this, &AOCPlayerPawn::InputLockCameraRotation);
	EnhancedInput->BindAction(InputConfigData->RotateCameraMouseDragY, ETriggerEvent::Triggered, this, &AOCPlayerPawn::InputRotateCameraByY);
	EnhancedInput->BindAction(InputConfigData->RotateCameraMouseDragZ, ETriggerEvent::Triggered, this, &AOCPlayerPawn::InputRotateCameraByZ);
	
	EnhancedInput->BindAction(InputConfigData->OpenOptionsMenu, ETriggerEvent::Started, this, &AOCPlayerPawn::InputOpenOptionsMenu);
}

void AOCPlayerPawn::SetCanMovePieces(bool bCanMove)
{
	bCanMovePieces = bCanMove;
}

void AOCPlayerPawn::SetupColor(EPieceColor PieceColor)
{
	Color = PieceColor;
}

void AOCPlayerPawn::Client_RotateBoardView_Implementation()
{
	const FTimerDelegate TimerDelegate_StartGameBoardRotation = FTimerDelegate::CreateWeakLambda(this, [this]()
	{
		const FRotator WhiteRotator = FRotator(0.0f);
		const FRotator BlackRotator = FRotator(0.0f, 180.0f, 0.0f);

		AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
		if (Board)
		{
			if (Color == EPieceColor::Black)
			{
				Board->SetActorRotation(BlackRotator);
			}
			else
			{
				Board->SetActorRotation(WhiteRotator);
			}
			GetWorldTimerManager().ClearTimer(TimerHandle_StartGameUpdate);
		}
	});

	GetWorldTimerManager().SetTimer(TimerHandle_StartGameUpdate, TimerDelegate_StartGameBoardRotation, StartGameUpdateRate, true);
}

void AOCPlayerPawn::InputSelectOrPlacePiece()
{
	if (bCanMovePieces)
	{
		InteractionComponent->SelectOrPlacePiece();
	}
}

void AOCPlayerPawn::InputHoldPiece()
{
	if (bCanMovePieces)
	{
		InteractionComponent->HoldPiece();
	}
}

void AOCPlayerPawn::InputPlacePiece()
{
	if (bCanMovePieces)
	{
		InteractionComponent->PlacePiece();
	}
}

void AOCPlayerPawn::InputDeselectPiece()
{
	if (bCanMovePieces)
	{
		InteractionComponent->DeselectPiece();
	}
}

void AOCPlayerPawn::InputZoomCamera(const FInputActionValue& AxisValue)
{
	EnhancedSpringArmComponent->ZoomCamera(AxisValue.Get<float>());
}

void AOCPlayerPawn::InputLockCameraRotation()
{
	EnhancedSpringArmComponent->LockCameraRotation();

	AOCPlayerController* PlayerController = Cast<AOCPlayerController>(GetController());
	PlayerController->FlipFlopMouseVisibility();
}

void AOCPlayerPawn::InputRotateCameraByY(const FInputActionValue& AxisValue)
{
	EnhancedSpringArmComponent->RotateCameraByY(AxisValue.Get<float>());
}

void AOCPlayerPawn::InputRotateCameraByZ(const FInputActionValue& AxisValue)
{
	EnhancedSpringArmComponent->RotateCameraByZ(AxisValue.Get<float>());
}

EPieceColor AOCPlayerPawn::GetColor() const
{
	return Color;
}

void AOCPlayerPawn::PlayCustomSound(EChessSoundType SoundType)
{
	Multicast_PlayCustomSound(SoundType);
}

void AOCPlayerPawn::Multicast_PlayCustomSound_Implementation(EChessSoundType SoundType)
{
	switch (SoundType)
	{
	case EChessSoundType::Move:
		UGameplayStatics::PlaySound2D(this, MoveSound);
		break;
	case EChessSoundType::Capture:
		UGameplayStatics::PlaySound2D(this, CaptureSound);
		break;
	case EChessSoundType::Check:
		UGameplayStatics::PlaySound2D(this, CheckSound);
		break;
	case EChessSoundType::Mate:
		UGameplayStatics::PlaySound2D(this, MateSound);
		break;
	case EChessSoundType::Login:
		UGameplayStatics::PlaySound2D(this, LoginSound);
		break;
	}
}

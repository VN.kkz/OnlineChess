// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/OCPlayerController.h"

AOCPlayerController::AOCPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}

void AOCPlayerController::FlipFlopMouseVisibility()
{
	bShowMouseCursor = !bShowMouseCursor;

	if (bShowMouseCursor)
	{
		SetMouseLocation(SavedMouseLocation.X,  SavedMouseLocation.Y);
	}
	else
	{
		GetMousePosition(SavedMouseLocation.X, SavedMouseLocation.Y);
	}
}

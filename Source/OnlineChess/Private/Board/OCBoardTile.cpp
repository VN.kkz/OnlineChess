// Fill out your copyright notice in the Description page of Project Settings.


#include "Board/OCBoardTile.h"

#include "Net/UnrealNetwork.h"

AOCBoardTile::AOCBoardTile()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	MeshComponent->SetCollisionObjectType(ECC_GameTraceChannel1);
	MeshComponent->SetupAttachment(RootComponent);
	
	bIsLegal = false;
}

void AOCBoardTile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AOCBoardTile, Piece);
	DOREPLIFETIME(AOCBoardTile, Coordinates);
}

void AOCBoardTile::SetCoordinates(int32 RankIndex, int32 FileIndex)
{
	Coordinates = FOCBoardCoordinates(RankIndex, FileIndex);
	
	SetColor();
}

FOCBoardCoordinates AOCBoardTile::GetCoordinates() const
{
	return Coordinates;
}

bool AOCBoardTile::IsLegal() const
{
	return bIsLegal;
}

void AOCBoardTile::SetHighlighted()
{
	if (DynamicMaterial)
	{
		DynamicMaterial->SetScalarParameterValue("IsHighlighted", 1.0f);
	}
}

void AOCBoardTile::SetInnerHighlighted()
{
	if (DynamicMaterial)
	{
		DynamicMaterial->SetScalarParameterValue("IsInnerHighlighted", 1.0f);
	}
}

void AOCBoardTile::SetWarningHighlighted()
{
	if (DynamicMaterial)
	{
		SetHighlighted();
		SetInnerHighlighted();
		DynamicMaterial->SetScalarParameterValue("IsWarningHighlighted", 1.0f);
	}
}

void AOCBoardTile::SetLegalMove()
{
	if (DynamicMaterial)
	{
		DynamicMaterial->SetScalarParameterValue("IsLegalMove", 1.0f);
	}
	bIsLegal = true;
}

void AOCBoardTile::SetLegalAttack()
{
	if (DynamicMaterial)
	{
		DynamicMaterial->SetScalarParameterValue("IsAttackMove", 1.0f);
	}
	bIsLegal = true;
}

void AOCBoardTile::Clear()
{
	if (DynamicMaterial)
	{
		DynamicMaterial->SetScalarParameterValue("IsHighlighted", 0.0f);
		DynamicMaterial->SetScalarParameterValue("IsInnerHighlighted", 0.0f);
		DynamicMaterial->SetScalarParameterValue("IsWarningHighlighted", 0.0f);
		DynamicMaterial->SetScalarParameterValue("IsLegalMove", 0.0f);
		DynamicMaterial->SetScalarParameterValue("IsAttackMove", 0.0f);
	}
	bIsLegal = false;
}

void AOCBoardTile::SetColor()
{
	if (DynamicMaterial == nullptr)
	{
		constexpr int32 MaterialIndex = 0;
		DynamicMaterial = UMaterialInstanceDynamic::Create(MeshComponent->GetMaterial(MaterialIndex), this);
		MeshComponent->SetMaterial(MaterialIndex, DynamicMaterial);
	}
	
	constexpr int32 NumColors = 2;
	const bool bIsFileEven = (Coordinates.File % NumColors) == 0;
	if (Coordinates.Rank % NumColors == 0)
	{
		if (bIsFileEven)
		{
			DynamicMaterial->SetScalarParameterValue(TEXT("IsWhite"), 0.0f);
		}
		else
		{
			DynamicMaterial->SetScalarParameterValue(TEXT("IsWhite"), 1.0f);
		}
	}
	else
	{
		if (bIsFileEven)
		{
			DynamicMaterial->SetScalarParameterValue(TEXT("IsWhite"), 1.0f);
		}
		else
		{
			DynamicMaterial->SetScalarParameterValue(TEXT("IsWhite"), 0.0f);
		}
	}
}

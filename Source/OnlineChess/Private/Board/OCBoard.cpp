// Fill out your copyright notice in the Description page of Project Settings.


#include "Board/OCBoard.h"

#include "Board/OCBoardTile.h"
#include "Components/OCBoardFENComponent.h"
#include "Net/UnrealNetwork.h"
#include "Pieces/OCPieceBase.h"

AOCBoard::AOCBoard()
{
	bReplicates = true;
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("BoardMesh");
	MeshComponent->SetupAttachment(RootComponent);

	FenComponent = CreateDefaultSubobject<UOCBoardFENComponent>("FenComponent");
	
	NumSquares = 8;
	SquareOffset = 80.0f;
	ScaleValue = 1.0f;
}

void AOCBoard::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AOCBoard, BoardTiles);
}

void AOCBoard::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	Construct();
	
	SetActorScale3D(FVector(ScaleValue));
}

int32 AOCBoard::GetNumSquares() const
{
	return NumSquares;
}

float AOCBoard::GetScaleValue() const
{
	return ScaleValue;
}

void AOCBoard::Construct()
{
	float OffsetMultiplier = NumSquares / 2;
	OffsetMultiplier = NumSquares % 2 == 0 ? OffsetMultiplier - 0.5f : OffsetMultiplier;
	
	FVector TileSpawnLocation;
	TileSpawnLocation.X = SquareOffset * OffsetMultiplier;
	TileSpawnLocation.Y = SquareOffset * -OffsetMultiplier;
	const FRotator TileSpawnRotation = GetActorRotation();
	
	for (int32 Rank = 0; Rank < NumSquares; ++Rank)
	{
		const float SavedRankLocation = TileSpawnLocation.X;
		TileSpawnLocation.X -= (Rank * SquareOffset);
		for (int32 File = NumSquares - 1; File >= 0; --File)
		{
			const float SavedFileLocation = TileSpawnLocation.Y;
			TileSpawnLocation.Y += (File * SquareOffset);
			
			UChildActorComponent* ChildActorComponent = Cast<UChildActorComponent>(
				AddComponentByClass(UChildActorComponent::StaticClass(), false,
					FTransform(TileSpawnRotation, TileSpawnLocation), false));
			if (ensure(BoardTileClass))
			{
				ChildActorComponent->SetChildActorClass(BoardTileClass);
			}

			AOCBoardTile* BoardTile = Cast<AOCBoardTile>(ChildActorComponent->GetChildActor());
			if (BoardTile)
			{
				BoardTile->SetCoordinates(Rank, NumSquares - File - 1);
			}

			TileSpawnLocation.Y = SavedFileLocation;
		}
		TileSpawnLocation.X = SavedRankLocation;
	}
}

void AOCBoard::InitSetup()
{
	TArray<UChildActorComponent*> ChildActorComponents;
	GetComponents<UChildActorComponent>(ChildActorComponents);
	ChildActorComponents.Sort([this](const UChildActorComponent& LeftChildActorComponent, const UChildActorComponent& RightChildActorComponent)
	{
		const AOCBoardTile* LeftBoardTile = Cast<AOCBoardTile>(LeftChildActorComponent.GetChildActor());
		const AOCBoardTile* RightBoardTile = Cast<AOCBoardTile>(RightChildActorComponent.GetChildActor());
		if (LeftBoardTile && RightBoardTile)
		{
			return ((NumSquares * LeftBoardTile->GetCoordinates().Rank) + LeftBoardTile->GetCoordinates().File)
				> ((NumSquares * RightBoardTile->GetCoordinates().Rank) + RightBoardTile->GetCoordinates().File);
		}
		return false;
	});
	
	BoardTiles.Empty();
	for (int32 Rank = 0; Rank < NumSquares; ++Rank)
	{
		for (int32 File = 0; File < NumSquares; ++File)
		{
			const int32 BoardTileIndex = (NumSquares * Rank) + File;
			AOCBoardTile* BoardTile = Cast<AOCBoardTile>(ChildActorComponents[BoardTileIndex]->GetChildActor());
			if (BoardTile)
			{
				BoardTile->SetCoordinates(Rank, File);
				BoardTiles.Add(BoardTile);
			}
		}
	}

	SpawnPieces();
}

void AOCBoard::SpawnPieces()
{
	if (BoardTiles.IsEmpty())
	{
		return;
	}

	TMultiMap<TSubclassOf<AOCPieceBase>, EPieceColor> StartingPiecePositions = FenComponent->GetPiecePosition();

	int32 Rank = NumSquares - 1;
	int32 File = 0;
	for (const TPair<TSubclassOf<AOCPieceBase>, EPieceColor>& PiecePosition : StartingPiecePositions)
	{
		const int32 BoardTileIndex = (NumSquares * Rank) + File;
		if (BoardTiles.IsValidIndex(BoardTileIndex))
		{
			AOCBoardTile* BoardTile = BoardTiles[BoardTileIndex];
			if (BoardTile)
			{
				TSubclassOf<AOCPieceBase> PieceClass = PiecePosition.Key;
				if (PieceClass)
				{
					const FVector PieceSpawnLocation = BoardTile->GetActorLocation();
					const FRotator PieceSpawnRotation = BoardTile->GetActorRotation();
							
					FActorSpawnParameters PieceSpawnParameters;
					PieceSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

					AOCPieceBase* SpawnedPiece = GetWorld()->SpawnActor<AOCPieceBase>(
						PieceClass, PieceSpawnLocation, PieceSpawnRotation, PieceSpawnParameters);
					if (SpawnedPiece)
					{
						SpawnedPiece->SetColor(PiecePosition.Value);
						SpawnedPiece->SetCoordinates(Rank, File);
						SpawnedPiece->SetActorScale3D(GetActorScale3D());
						SpawnedPiece->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

						BoardTile->Piece = SpawnedPiece;
					}
				}
			}
		}
		
		if (++File == NumSquares)
		{
			File = 0;
			--Rank;
		}
	}
}

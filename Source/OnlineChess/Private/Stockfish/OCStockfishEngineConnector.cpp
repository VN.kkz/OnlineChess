﻿#include "Stockfish\OCStockfishEngineConnector.h"

OCStockfishEngineConnector::OCStockfishEngineConnector()
	: StartupInfo({ 0 })
	, SecurityAttributes({ 0 })
	, ProcessInformation({ 0 })
	, ExecutableFilename(FString("/stockfish.exe"))
	, SleepDelay(3000)
{
}

OCStockfishEngineConnector::~OCStockfishEngineConnector()
{
	if (!bHasBeenConnected)
	{
		return;
	}
	
	WriteFile(HandleInWrite, "quit\n", 5, &Write, nullptr);
	
	if (HandleInWrite != nullptr)
	{
		CloseHandle(HandleInWrite);
	}
	if (HandleInRead != nullptr)
	{
		CloseHandle(HandleInRead);
	}
	if (HandleOutWrite != nullptr)
	{
		CloseHandle(HandleOutWrite);
	}
	if (HandleOutRead != nullptr)
	{
		CloseHandle(HandleOutRead);
	}
	if (ProcessInformation.hProcess != nullptr)
	{
		 CloseHandle(ProcessInformation.hProcess);
	}
	if (ProcessInformation.hThread != nullptr)
	{
		CloseHandle(ProcessInformation.hThread);
	}
}

void OCStockfishEngineConnector::ConnectWithPath(FString Path)
{
	HandleInWrite = HandleInRead = HandleOutWrite = HandleOutRead = nullptr;
	SecurityAttributes.nLength = sizeof(SecurityAttributes);
	SecurityAttributes.bInheritHandle = TRUE;
	SecurityAttributes.lpSecurityDescriptor = nullptr;

	CreatePipe(&HandleOutRead, &HandleOutWrite, &SecurityAttributes, 0);
	CreatePipe(&HandleInRead, &HandleInWrite, &SecurityAttributes, 0);
         
	StartupInfo.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	StartupInfo.wShowWindow = SW_HIDE;
	StartupInfo.hStdInput = HandleInRead;
	StartupInfo.hStdOutput = HandleOutWrite;
	StartupInfo.hStdError = HandleOutWrite;

	Path += ExecutableFilename;
	CreateProcessA(nullptr, TCHAR_TO_ANSI(*Path), nullptr, nullptr, TRUE,
		0, nullptr, nullptr, &StartupInfo, &ProcessInformation);

	bHasBeenConnected = true;
}

FString OCStockfishEngineConnector::GetBestMove(FString Moves, int32 Depth)
{
	if (!bHasBeenConnected)
	{
		return "0";
	}
	
	FString AnalysisResult;
	Moves = FString::Format(TEXT("position startpos moves {0}\ngo depth {1}\n"), { Moves, FString::FromInt(Depth) });    

	WriteFile(HandleInWrite, TCHAR_TO_ANSI(*Moves), Moves.Len(), &Write, nullptr);
	Sleep(SleepDelay);
        
	PeekNamedPipe(HandleOutRead, Buffer, sizeof(Buffer), &Read, &Available, nullptr);   
	do
	{   
		ZeroMemory(Buffer, sizeof(Buffer));
		if (!ReadFile(HandleOutRead, Buffer, sizeof(Buffer), &Read, nullptr) || !Read)
		{
			break;
		}
		Buffer[Read] = 0;    
		AnalysisResult += (char*)Buffer;
	} while (Read >= sizeof(Buffer));

	const int BestMoveStartIndex = AnalysisResult.Find("bestmove");  
	if (BestMoveStartIndex !=- 1)
	{
		return AnalysisResult.Mid(BestMoveStartIndex + 9, 4);
	}
             
	return "-1";
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Pieces/OCPieceRook.h"

#include "Components/OCPieceMovementComponent.h"

AOCPieceRook::AOCPieceRook()
{
	Type = EPieceType::Rook;

	MoveDirections = { EPieceMoveDirection::Top, EPieceMoveDirection::Right,
		EPieceMoveDirection::Bottom, EPieceMoveDirection::Left };
}

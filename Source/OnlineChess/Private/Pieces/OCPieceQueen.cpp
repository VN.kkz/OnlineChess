// Fill out your copyright notice in the Description page of Project Settings.


#include "Pieces/OCPieceQueen.h"

#include "Components/OCPieceMovementComponent.h"

AOCPieceQueen::AOCPieceQueen()
{
	Type = EPieceType::Queen;

	for (const EPieceMoveDirection Direction : TEnumRange<EPieceMoveDirection>())
	{
		MoveDirections.Add(Direction);
	}
}

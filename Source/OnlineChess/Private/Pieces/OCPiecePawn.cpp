// Fill out your copyright notice in the Description page of Project Settings.


#include "Pieces/OCPiecePawn.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "Board/OCBoardTile.h"
#include "Components/OCPieceMovementComponent.h"

AOCPiecePawn::AOCPiecePawn()
{
	Type = EPieceType::Pawn;

	bIsFirstMove = true;
}

bool AOCPiecePawn::MoveTo(AOCBoardTile* BoardTile)
{
	const bool bWasPiecedMoved = Super::MoveTo(BoardTile);
	if (bWasPiecedMoved)
	{
		bIsFirstMove = false;
	}
	return bWasPiecedMoved;
}

TArray<AOCBoardTile*> AOCPiecePawn::GetLegalMoves(bool bFilterMoves /* = true */)
{
	TArray<AOCBoardTile*> TempMoves;
	if (Color == EPieceColor::White)
	{
		MovementComponent->GetPossibleMoves(TempMoves, EPieceMoveDirection::Top, false);
	}
	else
	{
		MovementComponent->GetPossibleMoves(TempMoves, EPieceMoveDirection::Bottom, false);
	}
	
	TArray<AOCBoardTile*> LegalMoves;
	
	for (int32 i = 0; i < (bIsFirstMove ? 2 : 1); ++i)
	{
		if (TempMoves.IsValidIndex(i))
		{
			LegalMoves.Add(TempMoves[i]);
		}
	}
	
	if (bFilterMoves)
	{
		return FilterMovesByCheck(LegalMoves);
	}
	return LegalMoves;
}

TArray<AOCBoardTile*> AOCPiecePawn::GetAttackMoves(bool bFilterMoves /* = true */)
{
	TArray<AOCBoardTile*> AttackMoves;
	
	const AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
	const int32 NumSquares = Board->GetNumSquares();

	for (const FOCBoardCoordinates Tile : GetSpecificMoves())
	{
		if (Tile.Rank != FMath::Clamp(Tile.Rank, 0, NumSquares - 1) ||
		Tile.File != FMath::Clamp(Tile.File, 0, NumSquares - 1))
		{
			continue;
		}
		
		TArray<AOCBoardTile*> BoardTiles = Board->BoardTiles;
		const int32 BoardTileIndex = (NumSquares * Tile.Rank) + Tile.File;
		if (BoardTiles.IsValidIndex(BoardTileIndex))
		{
			AOCBoardTile* BoardTile = BoardTiles[BoardTileIndex];
			if (MovementComponent->IsEnemy(BoardTile->Piece))
			{
				AttackMoves.Add(BoardTile);
			}
		}
	}

	if (bFilterMoves)
	{
		return FilterMovesByCheck(AttackMoves);
	}
	return AttackMoves;
}

TArray<FOCBoardCoordinates> AOCPiecePawn::GetSpecificMoves() const
{
	const int32 MoveDirectionMultiplier = Color == EPieceColor::White ? 1 : -1;
	
	const FOCBoardCoordinates TileCoordinateLeft = FOCBoardCoordinates(
		Coordinates.Rank + MoveDirectionMultiplier, Coordinates.File + 1);
	const FOCBoardCoordinates TileCoordinateRight = FOCBoardCoordinates(
		Coordinates.Rank + MoveDirectionMultiplier, Coordinates.File - 1);

	return { TileCoordinateLeft, TileCoordinateRight };
}

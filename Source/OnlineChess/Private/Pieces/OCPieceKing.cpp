// Fill out your copyright notice in the Description page of Project Settings.


#include "Pieces/OCPieceKing.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "Pieces/OCPiecePawn.h"
#include "Components/OCPieceMovementComponent.h"
#include "Board/OCBoardTile.h"

AOCPieceKing::AOCPieceKing()
{
	Type = EPieceType::King;

	for (const EPieceMoveDirection Direction : TEnumRange<EPieceMoveDirection>())
	{
		MoveDirections.Add(Direction);
	}
}

TArray<AOCBoardTile*> AOCPieceKing::GetLegalMoves(bool bFilterMoves /* = true */)
{
	TArray<AOCBoardTile*> LegalMoves;
	
	constexpr int32 CutOffIndex = 0;
	
	TArray<AOCBoardTile*> TempMoves;
	for (const EPieceMoveDirection Direction : MoveDirections)
	{
		MovementComponent->GetPossibleMoves(TempMoves, Direction, false);
		if (TempMoves.IsValidIndex(CutOffIndex))
		{
			LegalMoves.Add(TempMoves[CutOffIndex]);
		}
		TempMoves.Empty();
	}
	
	if (bFilterMoves)
	{
		return FilterMovesByCheck(LegalMoves);
	}
	return LegalMoves;
}

TArray<AOCBoardTile*> AOCPieceKing::GetAttackMoves(bool bFilterMoves /* = true */)
{
	TArray<AOCBoardTile*> AttackMoves;
	
	const AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
	const int32 NumSquares = Board->GetNumSquares();

	for (const FOCBoardCoordinates Tile : GetSpecificMoves())
	{
		if (Tile.Rank != FMath::Clamp(Tile.Rank, 0, NumSquares - 1) ||
		Tile.File != FMath::Clamp(Tile.File, 0, NumSquares - 1))
		{
			continue;
		}
		
		TArray<AOCBoardTile*> BoardTiles = Board->BoardTiles;
		const int32 BoardTileIndex = (NumSquares * Tile.Rank) + Tile.File;
		if (BoardTiles.IsValidIndex(BoardTileIndex))
		{
			AOCBoardTile* BoardTile = BoardTiles[BoardTileIndex];
			if (MovementComponent->IsEnemy(BoardTile->Piece))
			{
				AttackMoves.Add(BoardTile);
			}
		}
	}

	if (bFilterMoves)
	{
		return FilterMovesByCheck(AttackMoves);
	}
	return AttackMoves;
}

TArray<FOCBoardCoordinates> AOCPieceKing::GetSpecificMoves() const
{
	const FOCBoardCoordinates TileCoordinateTop = FOCBoardCoordinates(
		Coordinates.Rank + 1, Coordinates.File);
	const FOCBoardCoordinates TileCoordinateTopLeft = FOCBoardCoordinates(
		Coordinates.Rank + 1, Coordinates.File - 1);
	const FOCBoardCoordinates TileCoordinateTopRight = FOCBoardCoordinates(
		Coordinates.Rank + 1, Coordinates.File - 1);
	const FOCBoardCoordinates TileCoordinateRight = FOCBoardCoordinates(
		Coordinates.Rank, Coordinates.File + 1);
	const FOCBoardCoordinates TileCoordinateBottom = FOCBoardCoordinates(
		Coordinates.Rank - 1, Coordinates.File);
	const FOCBoardCoordinates TileCoordinateBottomLeft = FOCBoardCoordinates(
		Coordinates.Rank - 1, Coordinates.File - 1);
	const FOCBoardCoordinates TileCoordinateBottomRight = FOCBoardCoordinates(
		Coordinates.Rank - 1, Coordinates.File + 1);
	const FOCBoardCoordinates TileCoordinateLeft = FOCBoardCoordinates(
		Coordinates.Rank, Coordinates.File - 1);
	
	return { TileCoordinateTop, TileCoordinateTopLeft, TileCoordinateTopRight,
		TileCoordinateRight, TileCoordinateBottom, TileCoordinateBottomLeft,
		TileCoordinateBottomRight, TileCoordinateLeft };
}

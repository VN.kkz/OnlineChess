// Fill out your copyright notice in the Description page of Project Settings.


#include "Pieces/OCPieceKnight.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "Components/OCPieceMovementComponent.h"
#include "Board/OCBoardTile.h"

AOCPieceKnight::AOCPieceKnight()
{
	Type = EPieceType::Knight;
}

TArray<AOCBoardTile*> AOCPieceKnight::GetPossibleTiles(bool bCheckForAttackMoves) const
{
	TArray<AOCBoardTile*> PossibleTiles;
	
	const AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
	const int32 NumSquares = Board->GetNumSquares();

	for (const FOCBoardCoordinates Tile : GetSpecificMoves())
	{
		if (Tile.Rank != FMath::Clamp(Tile.Rank, 0, NumSquares - 1) ||
		Tile.File != FMath::Clamp(Tile.File, 0, NumSquares - 1))
		{
			continue;
		}
		
		TArray<AOCBoardTile*> BoardTiles = Board->BoardTiles;
		const int32 BoardTileIndex = (NumSquares * Tile.Rank) + Tile.File;
		if (BoardTiles.IsValidIndex(BoardTileIndex))
		{
			AOCBoardTile* BoardTile = BoardTiles[BoardTileIndex];
			if (bCheckForAttackMoves && MovementComponent->IsEnemy(BoardTile->Piece))
			{
				PossibleTiles.Add(BoardTile);
			}
			else if (!bCheckForAttackMoves && BoardTile->Piece == nullptr)
			{
				PossibleTiles.Add(BoardTile);
			}
		}
	}

	return PossibleTiles;
}

TArray<FOCBoardCoordinates> AOCPieceKnight::GetSpecificMoves() const
{
	const FOCBoardCoordinates TileCoordinateUpLeft = FOCBoardCoordinates(
		Coordinates.Rank + 2, Coordinates.File - 1);
	const FOCBoardCoordinates TileCoordinateUpRight = FOCBoardCoordinates(
		Coordinates.Rank + 2, Coordinates.File + 1);
	const FOCBoardCoordinates TileCoordinateRightUp = FOCBoardCoordinates(
		Coordinates.Rank + 1, Coordinates.File + 2);
	const FOCBoardCoordinates TileCoordinateRightBottom = FOCBoardCoordinates(
		Coordinates.Rank - 1, Coordinates.File + 2);
	const FOCBoardCoordinates TileCoordinateBottomLeft = FOCBoardCoordinates(
		Coordinates.Rank - 2, Coordinates.File - 1);
	const FOCBoardCoordinates TileCoordinateBottomRight = FOCBoardCoordinates(
		Coordinates.Rank - 2, Coordinates.File + 1);
	const FOCBoardCoordinates TileCoordinateLeftUp = FOCBoardCoordinates(
		Coordinates.Rank + 1, Coordinates.File - 2);
	const FOCBoardCoordinates TileCoordinateLeftBottom = FOCBoardCoordinates(
	Coordinates.Rank - 1, Coordinates.File - 2);
	
	return { TileCoordinateUpLeft, TileCoordinateUpRight, TileCoordinateRightUp,
		TileCoordinateRightBottom, TileCoordinateBottomLeft, TileCoordinateBottomRight,
		TileCoordinateLeftUp, TileCoordinateLeftBottom };
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Pieces/OCPieceBase.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "Board/OCBoardTile.h"
#include "Components/OCPieceMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include "Pieces/OCPieceKing.h"
#include "Player/OCPlayerPawn.h"

AOCPieceBase::AOCPieceBase()
{
	bReplicates = true;
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("PieceMesh");
	MeshComponent->SetCollisionObjectType(ECC_GameTraceChannel1);
	MeshComponent->SetupAttachment(RootComponent);

	MovementComponent = CreateDefaultSubobject<UOCPieceMovementComponent>("PieceMovementComponent");

	Color = EPieceColor::Unknown;
}

void AOCPieceBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AOCPieceBase, Coordinates);
	DOREPLIFETIME(AOCPieceBase, Color);
}

void AOCPieceBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	
	if (DynamicMaterial == nullptr)
	{
		constexpr int32 MaterialIndex = 0;
		DynamicMaterial = MeshComponent->CreateAndSetMaterialInstanceDynamic(MaterialIndex);
	}
}

TArray<AOCBoardTile*> AOCPieceBase::GetMoves()
{
	TArray<AOCBoardTile*> Moves;
	
	Moves.Append(GetLegalMoves());
	Moves.Append(GetAttackMoves());

	return Moves;
}

TArray<AOCBoardTile*> AOCPieceBase::GetLegalMoves(bool bFilterMoves /* = true */)
{
	if (bFilterMoves)
	{
		return FilterMovesByCheck(GetPossibleTiles(false));
	}
	return GetPossibleTiles(false);
}

TArray<AOCBoardTile*> AOCPieceBase::GetAttackMoves(bool bFilterMoves /* = true */)
{
	if (bFilterMoves)
	{
		return FilterMovesByCheck(GetPossibleTiles(true));
	}
	return GetPossibleTiles(true);
}

TArray<AOCBoardTile*> AOCPieceBase::GetPossibleTiles(bool bCheckForAttackMoves) const
{
	TArray<AOCBoardTile*> PossibleTiles;

	for (const EPieceMoveDirection Direction : MoveDirections)
	{
		MovementComponent->GetPossibleMoves(PossibleTiles, Direction, bCheckForAttackMoves);
	}

	return PossibleTiles;
}

TArray<AOCBoardTile*> AOCPieceBase::FilterMovesByCheck(TArray<AOCBoardTile*> Moves)
{
	AOCBoardTile* CurrentBoardTile = FindCurrentBoardTile();
	CurrentBoardTile->Piece = nullptr;
	
	TArray<AOCBoardTile*> FilteredMoves;
	for (AOCBoardTile* BoardTile : Moves)
	{
		AOCPieceBase* SavedPiece = BoardTile->Piece;
		BoardTile->Piece = this;

		if (!IsAllyKingInCheck())
		{
			FilteredMoves.Add(BoardTile);
		}
		
		BoardTile->Piece = SavedPiece;
	}
	
	CurrentBoardTile->Piece = this;

	return FilteredMoves;
}

AOCBoardTile* AOCPieceBase::FindCurrentBoardTile() const
{
	for (AOCBoardTile* Tile : UOCGameplayFunctionLibrary::GetBoard(this)->BoardTiles)
	{
		if (Tile->GetCoordinates() == GetCoordinates())
		{
			return Tile;
		}
	}
	return nullptr;
}

bool AOCPieceBase::IsAllyKingInCheck() const
{
	const EPieceColor EnemyColor = UOCGameplayFunctionLibrary::GetOppositeColor(GetColor());
	
	for (AOCBoardTile* Tile : UOCGameplayFunctionLibrary::GetBoard(this)->BoardTiles)
	{
		AOCPieceBase* PieceBase = Cast<AOCPieceBase>(Tile->Piece);
		if (PieceBase && PieceBase->GetColor() == EnemyColor)
		{
			TArray<AOCBoardTile*> OppositeAttackTiles = PieceBase->GetAttackMoves(false);
			for (const AOCBoardTile* AttackTile : OppositeAttackTiles)
			{
				const AOCPieceKing* KingPiece = Cast<AOCPieceKing>(AttackTile->Piece);
				if (KingPiece)
				{
					return true;
				}
			}
		}
	}

	return false;
}

void AOCPieceBase::Multicast_PlaceAtTile_Implementation(AOCBoardTile* BoardTile)
{
	if (BoardTile)
	{
		SetActorLocation(BoardTile->GetActorLocation());
	}
}

void AOCPieceBase::OnRep_ColorHasChanged()
{
	if (Color == EPieceColor::Black)
	{
		DynamicMaterial->SetScalarParameterValue(TEXT("IsWhite"), 0.0f);
		
		constexpr float MirrorPieceRotationValue = 180.0f;
		const FRotator PieceRotation = FRotator(0.0f, MirrorPieceRotationValue, 0.0f);
		AddActorWorldRotation(PieceRotation);
	}
	else
	{
		DynamicMaterial->SetScalarParameterValue(TEXT("IsWhite"), 1.0f);
	}
}

bool AOCPieceBase::CanMoveTo(AOCBoardTile* BoardTile)
{
	const AOCBoard* Board = UOCGameplayFunctionLibrary::GetBoard(this);
	if (Board && BoardTile)
	{
		if (BoardTile->Piece != this)
		{
			return GetMoves().Contains(BoardTile);
		}
	}
	return false;
}

bool AOCPieceBase::MoveTo(AOCBoardTile* BoardTile)
{
	if (CanMoveTo(BoardTile))
	{
		if (BoardTile->Piece && GetAttackMoves().Contains(BoardTile))
		{
			BoardTile->Piece->Destroy();
			
			UOCGameplayFunctionLibrary::PlaySoundForAllPlayers(this, EChessSoundType::Capture);
		}
		
		const FOCBoardCoordinates NewPieceCoordinates = BoardTile->GetCoordinates();
		SetCoordinates(NewPieceCoordinates.Rank, NewPieceCoordinates.File);

		return true;
	}
	return false;
}

void AOCPieceBase::SetColor(EPieceColor PieceColor)
{
	Color = PieceColor;
	OnRep_ColorHasChanged();
}

void AOCPieceBase::SetCoordinates(int32 RankIndex, int32 FileIndex)
{
	Coordinates = FOCBoardCoordinates(RankIndex, FileIndex);
}

FOCBoardCoordinates AOCPieceBase::GetCoordinates() const
{
	return Coordinates;
}

EPieceColor AOCPieceBase::GetColor() const
{
	return Color;
}

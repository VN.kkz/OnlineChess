// Fill out your copyright notice in the Description page of Project Settings.


#include "Pieces/OCPieceBishop.h"

#include "Pieces/OCPieceBase.h"
#include "Components/OCPieceMovementComponent.h"

AOCPieceBishop::AOCPieceBishop()
{
	Type = EPieceType::Bishop;

	MoveDirections = { EPieceMoveDirection::TopLeft, EPieceMoveDirection::TopRight,
		EPieceMoveDirection::BottomLeft, EPieceMoveDirection::BottomRight };
}

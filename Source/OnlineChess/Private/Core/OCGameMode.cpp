// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/OCGameMode.h"

#include "Core/OCGameplayFunctionLibrary.h"
#include "Core/OCGameState.h"
#include "Board/OCBoard.h"
#include "Board/OCBoardTile.h"
#include "Core/OCGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Pieces/OCPieceBase.h"
#include "Player/OCPlayerPawn.h"

void AOCGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	FirstTurnColor = EPieceColor::White;
	CurrentTurnColor = EPieceColor::White;

	AvailableColors = { EPieceColor::White, EPieceColor::Black };

	StockfishEngine.ConnectWithPath(FPaths::GetPath(FPaths::GetProjectFilePath()));

	StockfishMoveDelay = 1.0f;
}

void AOCGameMode::InitGameState()
{
	Super::InitGameState();

	AOCGameState* OnlineChessGameState = GetGameState<AOCGameState>();
	OnlineChessGameState->Board = Cast<AOCBoard>(UGameplayStatics::GetActorOfClass(this, AOCBoard::StaticClass()));
	if (OnlineChessGameState->Board)
	{
		OnlineChessGameState->Board->InitSetup();
	}
}

void AOCGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	
	AOCPlayerPawn* PlayerPawn = Cast<AOCPlayerPawn>(NewPlayer->GetPawn());
	if (PlayerPawn)
	{
		const UOCGameInstance* GameInstance = Cast<UOCGameInstance>(GetGameInstance());
		if (GameInstance->CurrentGameType == EGameType::FreePlay)
		{
			const EPieceColor PlayerColor = GetFirstAvailableColor();
			PlayerPawn->SetupColor(PlayerColor);	
			PlayerPawn->SetCanMovePieces(true);
		}
		else if (GameInstance->CurrentGameType == EGameType::Computer)
		{
			const EPieceColor PlayerColor = GetRandomAvailableColor();
			PlayerPawn->SetupColor(PlayerColor);
			PlayerPawn->SetCanMovePieces(true);
			PlayerPawn->Client_RotateBoardView();

			if (PlayerColor == EPieceColor::Black)
			{
				PerformAI();
			}
		}
		else
		{
			const EPieceColor PlayerColor = GetRandomAvailableColor();
			PlayerPawn->SetupColor(PlayerColor);
			PlayerPawn->SetCanMovePieces(PlayerColor == FirstTurnColor);
			PlayerPawn->Client_RotateBoardView();
		}

		UOCGameplayFunctionLibrary::PlaySoundForAllPlayers(this, EChessSoundType::Login);
	}
}

bool AOCGameMode::ProceedNextTurn()
{
	if (IsCheckmate())
	{
		CurrentTurnColor = EPieceColor::Unknown;

		UOCGameplayFunctionLibrary::PlaySoundForAllPlayers(this, EChessSoundType::Mate);
	}
	else
	{
		CurrentTurnColor = UOCGameplayFunctionLibrary::GetOppositeColor(CurrentTurnColor);
	}
	
	const UOCGameInstance* GameInstance = Cast<UOCGameInstance>(GetGameInstance());
	if (GameInstance->CurrentGameType == EGameType::FreePlay)
	{
		AOCPlayerPawn* PlayerPawn = Cast<AOCPlayerPawn>(UGameplayStatics::GetActorOfClass(this, AOCPlayerPawn::StaticClass()));
		if (PlayerPawn)
		{
			PlayerPawn->SetupColor(CurrentTurnColor);
		}
	}
	else if (GameInstance->CurrentGameType == EGameType::Multiplayer)
	{
		if (GameInstance->GetMaxLobbyPlayers() == GetNumPlayers())
		{
			SetMoveAvailabilityForAllPlayers();
		}
	}
	else if (GameInstance->CurrentGameType == EGameType::Computer)
	{
		PerformAI();
	}
	else
	{
		return false;
	}
	
	return true;
}

EPieceColor AOCGameMode::GetFirstAvailableColor()
{
	EPieceColor FirstAvailableColor = EPieceColor::Unknown;
	
	constexpr int32 FirstIndex = 0;
	if (AvailableColors.IsValidIndex(FirstIndex))
	{
		FirstAvailableColor = AvailableColors[FirstIndex];
		AvailableColors.Remove(FirstAvailableColor);
	}
	
	return FirstAvailableColor;
}

EPieceColor AOCGameMode::GetRandomAvailableColor()
{
	EPieceColor RandomAvailableColor = EPieceColor::Unknown;
	
	const int32 RandomIndex = FMath::RandRange(0, AvailableColors.Num());
	if (AvailableColors.IsValidIndex(RandomIndex))
	{
		RandomAvailableColor = AvailableColors[RandomIndex];
		AvailableColors.Remove(RandomAvailableColor);
	}
	
	return RandomAvailableColor;
}

void AOCGameMode::SetMoveAvailabilityForAllPlayers()
{
	TArray<AActor*> PlayerPawns;
	UGameplayStatics::GetAllActorsOfClass(this, AOCPlayerPawn::StaticClass(),PlayerPawns);

	for (AActor* PlayerActor : PlayerPawns)
	{
		AOCPlayerPawn* PlayerPawn = Cast<AOCPlayerPawn>(PlayerActor);
		if (PlayerPawn)
		{
			PlayerPawn->SetCanMovePieces(PlayerPawn->GetColor() == CurrentTurnColor);
		}
	}
}

bool AOCGameMode::IsCheckmate() const
{
	const AOCGameState* OnlineChessGameState = GetGameState<AOCGameState>();
	for (const AOCBoardTile* BoardTile : OnlineChessGameState->Board->BoardTiles)
	{
		AOCPieceBase* Piece = BoardTile->Piece;
		if (Piece)
		{
			const EPieceColor OppositeColor = UOCGameplayFunctionLibrary::GetOppositeColor(Piece->GetColor());
			if (OppositeColor == CurrentTurnColor)
			{
				if (!Piece->GetMoves().IsEmpty())
				{
					return false;
				}
			}
		}
	}
	return true;
}

void AOCGameMode::PerformAI()
{
	AOCPlayerPawn* PlayerPawn = Cast<AOCPlayerPawn>(UGameplayStatics::GetActorOfClass(this, AOCPlayerPawn::StaticClass()));
	if (PlayerPawn)
	{
		PlayerPawn->SetCanMovePieces(false);

		FTimerDelegate TimerDelegate_StockfishMove = FTimerDelegate::CreateWeakLambda(this, [this, PlayerPawn]()
		{
			ProceedNextMoveAI();
				
			PlayerPawn->SetCanMovePieces(true);
		});

		GetWorldTimerManager().SetTimer(Stockfish_TimerHandle, TimerDelegate_StockfishMove, StockfishMoveDelay, false);
	}
}

void AOCGameMode::ProceedNextMoveAI()
{
	const FString MovesHistory = UOCGameplayFunctionLibrary::GetMovesHistoryString(this);
	const FString BestMove = StockfishEngine.GetBestMove(MovesHistory, 20);
	UE_LOG(LogTemp, Log, TEXT("Best Move: %s"), *BestMove);

	const TPair<AOCBoardTile*, AOCBoardTile*> Tiles = UOCGameplayFunctionLibrary::GetBoardTilesFromNotation(this, BestMove);
	UOCGameplayFunctionLibrary::MakeMove(Tiles.Key->Piece, Tiles.Value, Tiles.Key, false, true);
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/OCGameInstance.h"

UOCGameInstance::UOCGameInstance()
{
	MasterAudioVolume = 1.0f;
	CurrentGameType = EGameType::Multiplayer;
	MaxLobbyPlayers = 2;
}

int32 UOCGameInstance::GetMaxLobbyPlayers() const
{
	return MaxLobbyPlayers;
}

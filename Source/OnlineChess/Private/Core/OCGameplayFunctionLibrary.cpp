// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/OCGameplayFunctionLibrary.h"

#include "Core/OCGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Core/OCGameState.h"
#include "Pieces/OCPieceBase.h"
#include "Board/OCBoardTile.h"
#include "Pieces/OCPieceKing.h"
#include "Player/OCPlayerPawn.h"

AOCGameMode* UOCGameplayFunctionLibrary::GetGameMode(const UObject* Instigator)
{
	return Cast<AOCGameMode>(UGameplayStatics::GetGameMode(Instigator));
}

AOCGameState* UOCGameplayFunctionLibrary::GetGameState(const UObject* Instigator)
{
	return Cast<AOCGameState>(UGameplayStatics::GetGameState(Instigator));
}

AOCBoard* UOCGameplayFunctionLibrary::GetBoard(const UObject* Instigator)
{
	const AOCGameState* GameState = GetGameState(Instigator);
	if (GameState)
	{
		return GameState->GetBoard();
	}
	return nullptr;
}

float UOCGameplayFunctionLibrary::GetBoardScale(const UObject* Instigator)
{
	const AOCBoard* Board = GetBoard(Instigator);
	if (Board)
	{
		return Board->GetScaleValue();
	}
	return 1.0f;
}

TPair<AOCBoardTile*, AOCBoardTile*> UOCGameplayFunctionLibrary::GetBoardTilesFromNotation(const UObject* Instigator, const FString& MoveNotation)
{
	AOCBoardTile* FirstBoardTile = nullptr;
	AOCBoardTile* SecondBoardTile = nullptr;
	
	FString FirstBoardTileString = MoveNotation.LeftChop(2);
	FOCBoardCoordinates FirstBoardTileCoordinates;
	FirstBoardTileCoordinates.Rank = static_cast<int32>(FirstBoardTileString[1]) - 49;
	FirstBoardTileCoordinates.File = FirstBoardTileString[0] - 'a';
	
	FString SecondBoardTileString = MoveNotation.RightChop(2);
	FOCBoardCoordinates SecondBoardTileCoordinates;
	SecondBoardTileCoordinates.Rank = static_cast<int32>(SecondBoardTileString[1]) - 49;
	SecondBoardTileCoordinates.File = SecondBoardTileString[0] - 'a';

	for (AOCBoardTile* Tile : GetBoard(Instigator)->BoardTiles)
	{
		if (Tile->GetCoordinates() == FirstBoardTileCoordinates)
		{
			FirstBoardTile = Tile;
		}
		else if (Tile->GetCoordinates() == SecondBoardTileCoordinates)
		{
			SecondBoardTile = Tile;
		}
	}

	return TPair<AOCBoardTile*, AOCBoardTile*>(FirstBoardTile, SecondBoardTile);
}

TArray<AOCBoardTile*> UOCGameplayFunctionLibrary::GetLastMoves(const UObject* Instigator)
{
	const AOCGameState* GameState = GetGameState(Instigator);
	if (GameState)
	{
		return GameState->GetLastMoves();
	}
	return TArray<AOCBoardTile*>();
}

AOCBoardTile* UOCGameplayFunctionLibrary::GetCheckTile(const UObject* Instigator)
{
	const AOCGameState* GameState = GetGameState(Instigator);
	if (GameState)
	{
		return GameState->GetCheckTile();
	}
	return nullptr;
}

FString UOCGameplayFunctionLibrary::GetMovesHistoryString(const UObject* Instigator)
{
	FString MovesHistoryString = "-1";
	
	AOCGameState* GameState = GetGameState(Instigator);
	if (GameState)
	{
		MovesHistoryString = GameState->GetMovesHistoryString();
	}

	return MovesHistoryString;
}

void UOCGameplayFunctionLibrary::SetLastMoves(const UObject* Instigator, AOCBoardTile* BoardTileStart, AOCBoardTile* BoardTileEnd)
{
	AOCGameState* GameState = GetGameState(Instigator);
	if (GameState)
	{
		GameState->SetLastMoves(BoardTileStart, BoardTileEnd);
	}
}

void UOCGameplayFunctionLibrary::SetCheckTile(AOCPieceBase* PieceInstigator)
{
	AOCGameState* GameState = GetGameState(PieceInstigator);
	if (GameState == nullptr)
	{
		return;
	}
	
	for (const AOCBoardTile* Tile : GetBoard(PieceInstigator)->BoardTiles)
	{
		const AOCPieceBase* PieceBase = Cast<AOCPieceBase>(Tile->Piece);
		if (PieceBase && PieceBase->GetColor() == PieceInstigator->GetColor())
		{
			TArray<AOCBoardTile*> OppositeAttackTiles = PieceInstigator->GetAttackMoves(false);
			for (AOCBoardTile* AttackTile : OppositeAttackTiles)
			{
				const AOCPieceKing* KingPiece = Cast<AOCPieceKing>(AttackTile->Piece);
				if (KingPiece)
				{
					GameState->SetCheckTile(AttackTile);

					PlaySoundForAllPlayers(PieceInstigator, EChessSoundType::Check);
					
					return;
				}
			}
		}
	}
	
	GameState->SetCheckTile(nullptr);
}

void UOCGameplayFunctionLibrary::AddMoveToHistory(const UObject* Instigator, const FString& MoveString)
{
	AOCGameState* GameState = GetGameState(Instigator);
	if (GameState)
	{
		GameState->AddMoveToHistory(MoveString);
	}
}

void UOCGameplayFunctionLibrary::MakeMove(AOCPieceBase* Piece, AOCBoardTile* FirstBoardTile, AOCBoardTile* SecondBoardTile,
	bool bForcePositionReset, bool bPreventNextMove /* = false */)
{
	if (Piece)
	{
		AOCBoardTile* NewBoardTile = SecondBoardTile;

		if (!bForcePositionReset)
		{
			const bool bWasPiecedMoved = Piece->MoveTo(FirstBoardTile);
			if (bWasPiecedMoved)
			{
				FirstBoardTile->Piece = Piece;
				SecondBoardTile->Piece = nullptr;
			
				NewBoardTile = FirstBoardTile;
				SetLastMoves(Piece, FirstBoardTile, SecondBoardTile);
				SetCheckTile(Piece);
				
				const FString MoveString = FString::Format(TEXT("{0}{1}"),
					{ SecondBoardTile->GetCoordinates().GetCoordinatesNotation(), *FirstBoardTile->GetCoordinates().GetCoordinatesNotation() });
				AddMoveToHistory(Piece, MoveString);
				UE_LOG(LogTemp, Log, TEXT("%s"), *MoveString);

				if (!bPreventNextMove)
				{
					NextTurn(Piece);
				}

				PlaySoundForAllPlayers(Piece, EChessSoundType::Move);
			}
		}
		
		Piece->Multicast_PlaceAtTile(NewBoardTile);
	}
}

void UOCGameplayFunctionLibrary::PlaySoundForAllPlayers(const UObject* Instigator, EChessSoundType SoundType)
{
	TArray<AActor*> PlayerActors;
	UGameplayStatics::GetAllActorsOfClass(Instigator, AOCPlayerPawn::StaticClass(), PlayerActors);
	for (AActor* PlayerActor : PlayerActors)
	{
		AOCPlayerPawn* PlayerPawn = Cast<AOCPlayerPawn>(PlayerActor);
		if (PlayerPawn)
		{
			PlayerPawn->PlayCustomSound(SoundType);
		}
	}
}

bool UOCGameplayFunctionLibrary::NextTurn(const UObject* Instigator)
{
	return GetGameMode(Instigator)->ProceedNextTurn();
}

EPieceColor UOCGameplayFunctionLibrary::GetOppositeColor(EPieceColor PieceColor)
{
	for (const EPieceColor Color : TEnumRange<EPieceColor>())
	{
		if (Color != PieceColor)
		{
			return Color;
		}
	}
	return EPieceColor::Unknown;
}

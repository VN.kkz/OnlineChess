// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/OCGameState.h"

#include "Board/OCBoardTile.h"
#include "Net/UnrealNetwork.h"

void AOCGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AOCGameState, Board);
	DOREPLIFETIME(AOCGameState, LastMoves);
	DOREPLIFETIME(AOCGameState, CheckTile);
	DOREPLIFETIME(AOCGameState, MovesHistory);
}

AOCBoard* AOCGameState::GetBoard() const
{
	return Board;
}

TArray<AOCBoardTile*> AOCGameState::GetLastMoves() const
{
	return LastMoves;
}

void AOCGameState::SetLastMoves(AOCBoardTile* BoardTileStart, AOCBoardTile* BoardTileEnd)
{
	LastMoves.Empty();
	
	LastMoves.Add(BoardTileStart);
	LastMoves.Add(BoardTileEnd);
}

AOCBoardTile* AOCGameState::GetCheckTile() const
{
	return CheckTile;
}

void AOCGameState::SetCheckTile(AOCBoardTile* BoardTile)
{
	CheckTile = BoardTile;
}

void AOCGameState::AddMoveToHistory(const FString& MoveString)
{
	MovesHistory.Add(MoveString);
}

TArray<FString> AOCGameState::GetMovesHistory() const
{
	return MovesHistory;
}

FString AOCGameState::GetMovesHistoryString() const
{
	FString MovesHistoryString = "";
	for (const FString& Move : MovesHistory)
	{
		MovesHistoryString += Move + " ";
	}
	return MovesHistoryString.TrimEnd();
}

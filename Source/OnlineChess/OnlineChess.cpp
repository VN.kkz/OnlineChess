// Copyright Epic Games, Inc. All Rights Reserved.

#include "OnlineChess.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OnlineChess, "OnlineChess" );

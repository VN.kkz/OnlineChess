// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OCBoard.generated.h"

class UOCBoardFENComponent;
class AOCBoardTile;

UCLASS()
class ONLINECHESS_API AOCBoard : public AActor
{
	GENERATED_BODY()
	
public:	
	AOCBoard();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void OnConstruction(const FTransform& Transform) override;

	void InitSetup();

	int32 GetNumSquares() const;

	float GetScaleValue() const;
	
	UPROPERTY(Replicated, VisibleAnywhere)
	TArray<AOCBoardTile*> BoardTiles;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "ChessBoard")
	int32 NumSquares;
	
	UPROPERTY(EditInstanceOnly, Category = "ChessBoard")
	float SquareOffset;

	UPROPERTY(EditInstanceOnly, Category = "ChessBoard")
	float ScaleValue;
	
	UPROPERTY(EditDefaultsOnly, Category = "ChessBoard")
	TSubclassOf<AOCBoardTile> BoardTileClass;
	
	UPROPERTY(EditDefaultsOnly, Category = "FEN")
	UOCBoardFENComponent* FenComponent;
	
	UPROPERTY(EditDefaultsOnly, Category = "ChessBoard")
	UStaticMeshComponent* MeshComponent;

private:
	void Construct();
	
	void SpawnPieces();
};

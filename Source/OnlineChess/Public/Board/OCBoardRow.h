﻿#pragma once

#include "Engine/DataTable.h"
#include "OCBoardRow.generated.h"

class AOCPieceBase;

USTRUCT(BlueprintType)
struct FOCBoardRow : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ChessBoard")
	TArray<TSubclassOf<AOCPieceBase>> Files;
};

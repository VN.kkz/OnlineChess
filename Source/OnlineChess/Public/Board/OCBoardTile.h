// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OCBoardCoordinates.h"
#include "GameFramework/Actor.h"
#include "OCBoardTile.generated.h"

class AOCPieceBase;

UCLASS()
class ONLINECHESS_API AOCBoardTile : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Replicated)
	AOCPieceBase* Piece;
	
	AOCBoardTile();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	void SetCoordinates(int32 RowIndex, int32 ColumnIndex);
	
	FOCBoardCoordinates GetCoordinates() const;

	/*** Start of material functions ***/
	
	bool IsLegal() const;
	
	void SetHighlighted();

	void SetInnerHighlighted();

	void SetWarningHighlighted();

	void SetLegalMove();

	void SetLegalAttack();

	void Clear();

	/*** End of material functions ***/

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ChessBoard")
	UStaticMeshComponent* MeshComponent;
	
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicMaterial;

private:
	bool bIsLegal;
	
	UPROPERTY(Replicated)
	FOCBoardCoordinates Coordinates;

	void SetColor();
};

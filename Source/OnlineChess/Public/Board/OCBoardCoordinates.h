﻿#pragma once

#include "OCBoardCoordinates.generated.h"

USTRUCT(BlueprintType)
struct FOCBoardCoordinates
{
	GENERATED_BODY()

	UPROPERTY()
	int32 Rank;
	
	UPROPERTY()
	int32 File;

	FOCBoardCoordinates();
	
	FOCBoardCoordinates(int32 RankIndex, int32 FileIndex);

	FString GetCoordinatesNotation() const;

	bool operator==(FOCBoardCoordinates SecondBoardCoordinates) const;
};

inline FOCBoardCoordinates::FOCBoardCoordinates()
	: FOCBoardCoordinates(0, 0)
{
}

inline FOCBoardCoordinates::FOCBoardCoordinates(int32 RankIndex, int32 FileIndex)
	: Rank(RankIndex), File(FileIndex)
{
}

inline FString FOCBoardCoordinates::GetCoordinatesNotation() const
{
	const char FileNotation = static_cast<char>(static_cast<int32>('a') + File);
	
	FString NotationString;
	NotationString.AppendChar(FileNotation);
	NotationString.Append(FString::FromInt(Rank + 1));
	
	return NotationString;
}

inline bool FOCBoardCoordinates::operator==(FOCBoardCoordinates SecondBoardCoordinates) const
{
	return Rank == SecondBoardCoordinates.Rank && File == SecondBoardCoordinates.File;
}

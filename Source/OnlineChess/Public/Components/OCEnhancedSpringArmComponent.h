// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "OCEnhancedSpringArmComponent.generated.h"

/**
 * 
 */
UCLASS()
class ONLINECHESS_API UOCEnhancedSpringArmComponent : public USpringArmComponent
{
	GENERATED_BODY()
	
public:	
	UOCEnhancedSpringArmComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void BeginPlay() override;

	void ZoomCamera(float AxisValue);
	
	void LockCameraRotation();
	
	void RotateCameraByY(float AxisValue);
	
	void RotateCameraByZ(float AxisValue);

protected:
	UPROPERTY(EditAnywhere, Category = "Camera")
	float DefaultCameraZoom;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float MinCameraZoom;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float MaxCameraZoom;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float CameraZoomSpeed;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float CameraZoomScale;
	
	float CameraZoomDifference;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float DefaultCameraRotationDegree;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float MinCameraRotationDegree;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float MaxCameraRotationDegree;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float CameraRotationScale;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float CameraSideRotationScale;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	float CameraSideRotationSpeed;

private:
	bool bIsCameraRotating;
	
	void UpdateCameraLocation(float DeltaSeconds);
	
	void UpdateScaling();
};

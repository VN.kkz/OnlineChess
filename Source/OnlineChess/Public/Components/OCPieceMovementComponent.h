// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Board/OCBoardCoordinates.h"
#include "OCPieceMovementComponent.generated.h"

enum class EPieceMoveDirection : uint8
{
	Top,
	TopLeft,
	TopRight,
	Right,
	Bottom,
	BottomLeft,
	BottomRight,
	Left,
};

ENUM_RANGE_BY_VALUES(EPieceMoveDirection, EPieceMoveDirection::Top, EPieceMoveDirection::TopLeft,
	EPieceMoveDirection::TopRight, EPieceMoveDirection::Right, EPieceMoveDirection::Bottom,
	EPieceMoveDirection::BottomLeft, EPieceMoveDirection::BottomRight, EPieceMoveDirection::Left);

class AOCPieceBase;
class AOCBoardTile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ONLINECHESS_API UOCPieceMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	void GetPossibleMoves(TArray<AOCBoardTile*>& Moves, EPieceMoveDirection MoveDirection,
		bool bCheckForAttackMoves) const;
	
	bool IsAlly(AOCPieceBase* OtherPiece) const;
	
	bool IsEnemy(AOCPieceBase* OtherPiece) const;
	
protected:
	AOCPieceBase* GetOwnerPiece() const;
	
	FOCBoardCoordinates GetOwnerCoordinates() const;

private:
	FOCBoardCoordinates GetPossibleMoveCoordinates(EPieceMoveDirection MoveDirection, int32 MoveDistanceValue) const;

	bool AddPossibleMove(TArray<AOCBoardTile*>& PossibleMoves, bool bCheckForAttackMoves,
		FOCBoardCoordinates Coordinates) const;

	TPair<int32, int32> GetDirectionMultiplier(EPieceMoveDirection MoveDirection) const;
};

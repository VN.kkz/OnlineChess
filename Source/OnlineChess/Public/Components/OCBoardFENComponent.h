// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OCBoardFENComponent.generated.h"

class AOCPieceBase;
enum class EPieceType : uint8;
enum class EPieceColor : uint8;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ONLINECHESS_API UOCBoardFENComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UOCBoardFENComponent();

	TMultiMap<TSubclassOf<AOCPieceBase>, EPieceColor> GetPiecePosition();
	
protected:
	UPROPERTY(EditDefaultsOnly, Category = "FEN")
	FString StartingPiecesPosition;

	UPROPERTY(EditDefaultsOnly, Category = "FEN")
	TMap<EPieceType, TSubclassOf<AOCPieceBase>> PieceTypeClasses;

private:
	TSubclassOf<AOCPieceBase> GetFilteredPieceClass(char PieceChar) const;
	
	EPieceColor GetFilteredPieceColor(char PieceChar) const;
};

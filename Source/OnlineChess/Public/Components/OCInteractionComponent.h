// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OCInteractionComponent.generated.h"

class AOCBoard;
class AOCBoardTile;
class AOCPieceBase;
class APlayerController;
enum class EPieceColor : uint8_t;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ONLINECHESS_API UOCInteractionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UOCInteractionComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void BeginPlay() override;

	void SelectOrPlacePiece();
	
	void HoldPiece();

	void PlacePiece();

	void DeselectPiece();

protected:
	UPROPERTY()
	AOCPieceBase* SelectedPiece;
	
	UPROPERTY()
	AOCPieceBase* HoldingPiece;
	
	UPROPERTY()
	AOCBoardTile* SelectedBoardTile;
	
	UPROPERTY(EditDefaultsOnly, Category = "Input")
	float DraggingPieceOffsetX;
	
	UPROPERTY(EditDefaultsOnly, Category = "Input")
	float DraggingPieceOffsetZ;

	UPROPERTY(EditDefaultsOnly)
	float InteractionDistance;
	
	void SetBuffers(AOCPieceBase* Piece);
	
	void ClearBuffers();

private:
	UPROPERTY()
	TArray<AOCBoardTile*> LegalMovesBuffer;

	UPROPERTY()
	TArray<AOCBoardTile*> AttackMovesBuffer;
	
	APlayerController* GetOwnerPlayerController() const;

	EPieceColor GetOwnerPieceColor() const;
	
	AOCBoardTile* GetBoardTile(bool bIgnoreHoldingAndSelectedPieces) const;
	
	FHitResult LineTraceBoardTile(bool bIgnoreHoldingAndSelectedPieces) const;

	bool LockPiece(AOCPieceBase*& Piece);

	void UnlockPiece(AOCPieceBase*& Piece, bool bForcePositionReset = false);
	
	UFUNCTION(Server, Reliable)
	void Server_TryToMakeMove(AOCPieceBase* Piece, AOCBoardTile* FirstBoardTile,
		AOCBoardTile* SecondBoardTile, bool bForcePositionReset);

	void UpdatePieceLocation() const;

	void UpdateBoardTiles();
	
	void UpdateCursorAppearance() const;
};

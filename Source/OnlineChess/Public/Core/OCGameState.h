// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "OCGameState.generated.h"

enum class EPieceColor : uint8;
class AOCBoard;
class AOCBoardTile;

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	AOCBoard* GetBoard() const;

	TArray<AOCBoardTile*> GetLastMoves() const;
	
	void SetLastMoves(AOCBoardTile* BoardTileStart, AOCBoardTile* BoardTileEnd);
	
	AOCBoardTile* GetCheckTile() const;

	void SetCheckTile(AOCBoardTile* BoardTile);
	
	TArray<FString> GetMovesHistory() const;

	FString GetMovesHistoryString() const;
	
	void AddMoveToHistory(const FString& MoveString);
	
	UPROPERTY(Replicated)
	AOCBoard* Board;
	
protected:
	UPROPERTY(Replicated)
	TArray<AOCBoardTile*> LastMoves;

	UPROPERTY(Replicated)
	AOCBoardTile* CheckTile;

	UPROPERTY(Replicated)
	TArray<FString> MovesHistory;
};

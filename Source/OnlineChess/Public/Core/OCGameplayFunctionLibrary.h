// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Board/OCBoard.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "OCGameplayFunctionLibrary.generated.h"

enum class EChessSoundType : uint8;
enum class EPieceColor : uint8;
class AOCPieceBase;
class AOCBoard;
class AOCBoardTile;
class AOCGameMode;
class AOCGameState;

/**
 * 
 */
UCLASS()
class ONLINECHESS_API UOCGameplayFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static AOCBoard* GetBoard(const UObject* Instigator);
	
	static float GetBoardScale(const UObject* Instigator);

	static TPair<AOCBoardTile*, AOCBoardTile*> GetBoardTilesFromNotation(const UObject* Instigator, const FString& MoveNotation);
	
	static TArray<AOCBoardTile*> GetLastMoves(const UObject* Instigator);
	
	static AOCBoardTile* GetCheckTile(const UObject* Instigator);

	static FString GetMovesHistoryString(const UObject* Instigator);

	static void MakeMove(AOCPieceBase* Piece, AOCBoardTile* FirstBoardTile, AOCBoardTile* SecondBoardTile,
		bool bForcePositionReset, bool bPreventNextMove = false);

	static void PlaySoundForAllPlayers(const UObject* Instigator, EChessSoundType SoundType);

	static EPieceColor GetOppositeColor(EPieceColor PieceColor);

protected:
	static bool NextTurn(const UObject* Instigator);
	
	static AOCGameMode* GetGameMode(const UObject* Instigator);
	
	static AOCGameState* GetGameState(const UObject* Instigator);
	
	static void SetLastMoves(const UObject* Instigator, AOCBoardTile* BoardTileStart, AOCBoardTile* BoardTileEnd);
	
	static void SetCheckTile(AOCPieceBase* PieceInstigator);

	static void AddMoveToHistory(const UObject* Instigator, const FString& MoveString);
};

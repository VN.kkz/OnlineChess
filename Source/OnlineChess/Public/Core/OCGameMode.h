// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Stockfish/OCStockfishEngineConnector.h"
#include "OCGameMode.generated.h"

enum class EPieceColor : uint8_t;

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	
	virtual void InitGameState() override;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	bool ProceedNextTurn();

protected:
	EPieceColor GetFirstAvailableColor();

	EPieceColor GetRandomAvailableColor();

	void SetMoveAvailabilityForAllPlayers();

	EPieceColor FirstTurnColor;
	
	EPieceColor CurrentTurnColor;

	TArray<EPieceColor> AvailableColors;

	bool IsCheckmate() const;

private:
	UPROPERTY(EditDefaultsOnly)
	float StockfishMoveDelay;
	
	FTimerHandle Stockfish_TimerHandle;

	OCStockfishEngineConnector StockfishEngine;
	
	void ProceedNextMoveAI();

	void PerformAI();
};

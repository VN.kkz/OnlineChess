// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OCGameInstance.generated.h"

UENUM(BlueprintType)
enum class EGameType : uint8
{
	FreePlay,
	Multiplayer,
	Computer,
};

/**
 * 
 */
UCLASS()
class ONLINECHESS_API UOCGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UOCGameInstance();

	int32 GetMaxLobbyPlayers() const;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	EGameType CurrentGameType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float MasterAudioVolume;
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 MaxLobbyPlayers;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pieces/OCPieceBase.h"
#include "OCPieceBishop.generated.h"

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCPieceBishop : public AOCPieceBase
{
	GENERATED_BODY()

public:
	AOCPieceBishop();
};

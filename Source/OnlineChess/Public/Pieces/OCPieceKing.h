// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pieces/OCPieceBase.h"
#include "OCPieceKing.generated.h"

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCPieceKing : public AOCPieceBase
{
	GENERATED_BODY()

public:
	AOCPieceKing();

	virtual TArray<AOCBoardTile*> GetLegalMoves(bool bFilterMoves = true) override;

	virtual TArray<AOCBoardTile*> GetAttackMoves(bool bFilterMoves = true) override;

private:
	TArray<FOCBoardCoordinates> GetSpecificMoves() const;
};

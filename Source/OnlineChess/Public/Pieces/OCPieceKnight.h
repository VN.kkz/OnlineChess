// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pieces/OCPieceBase.h"
#include "OCPieceKnight.generated.h"

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCPieceKnight : public AOCPieceBase
{
	GENERATED_BODY()

public:
	AOCPieceKnight();

protected:
	virtual TArray<AOCBoardTile*> GetPossibleTiles(bool bCheckForAttackMoves) const override;

private:
	TArray<FOCBoardCoordinates> GetSpecificMoves() const;
};

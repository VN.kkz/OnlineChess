// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Board/OCBoardCoordinates.h"
#include "GameFramework/Actor.h"
#include "OCPieceBase.generated.h"

class UOCPieceMovementComponent;

UENUM(BlueprintType)
enum class EPieceColor : uint8
{
	White,
	Black,
	Unknown,
};

ENUM_RANGE_BY_COUNT(EPieceColor, EPieceColor::Unknown);

UENUM(BlueprintType)
enum class EPieceType : uint8
{
	Pawn,
	Bishop,
	Knight,
	Rook,
	Queen,
	King,
};

ENUM_RANGE_BY_VALUES(EPieceType, EPieceType::Pawn, EPieceType::Bishop, EPieceType::Knight,
	EPieceType::Rook, EPieceType::Queen, EPieceType::King);

enum class EPieceMoveDirection : uint8;

class AOCBoard;
class AOCBoardTile;

UCLASS(Abstract)
class ONLINECHESS_API AOCPieceBase : public AActor
{
	GENERATED_BODY()
	
public:
	AOCPieceBase();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void OnConstruction(const FTransform& Transform) override;

	virtual bool MoveTo(AOCBoardTile* BoardTile);

	TArray<AOCBoardTile*> GetMoves();

	virtual TArray<AOCBoardTile*> GetLegalMoves(bool bFilterMoves = true);

	virtual TArray<AOCBoardTile*> GetAttackMoves(bool bFilterMoves = true);

	void SetColor(EPieceColor PieceColor);
	
	void SetCoordinates(int32 RankIndex, int32 FileIndex);
	
	FOCBoardCoordinates GetCoordinates() const;

	EPieceColor GetColor() const;

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_PlaceAtTile(AOCBoardTile* BoardTile);

protected:
	UPROPERTY(Replicated)
	FOCBoardCoordinates Coordinates;

	TArray<EPieceMoveDirection> MoveDirections;

	UPROPERTY()
	UOCPieceMovementComponent* MovementComponent;
	
	UPROPERTY(ReplicatedUsing=OnRep_ColorHasChanged, VisibleAnywhere, Category = "ChessPiece")
	EPieceColor Color;
	
	UPROPERTY(VisibleAnywhere, Category = "ChessPiece")
	EPieceType Type;
	
	UPROPERTY(EditDefaultsOnly, Category = "ChessPiece")
	UStaticMeshComponent* MeshComponent;
	
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicMaterial;

	UFUNCTION()
	void OnRep_ColorHasChanged();

	bool CanMoveTo(AOCBoardTile* BoardTile);

	virtual TArray<AOCBoardTile*> GetPossibleTiles(bool bCheckForAttackMoves) const;

	TArray<AOCBoardTile*> FilterMovesByCheck(TArray<AOCBoardTile*> Moves);

	AOCBoardTile* FindCurrentBoardTile() const;
	
	bool IsAllyKingInCheck() const;
};

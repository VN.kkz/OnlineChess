// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pieces/OCPieceBase.h"
#include "OCPieceRook.generated.h"

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCPieceRook : public AOCPieceBase
{
	GENERATED_BODY()

public:
	AOCPieceRook();
};

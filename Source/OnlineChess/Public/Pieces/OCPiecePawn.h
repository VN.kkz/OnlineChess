// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pieces/OCPieceBase.h"
#include "OCPiecePawn.generated.h"

enum class EPieceMoveDirection : uint8;

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCPiecePawn : public AOCPieceBase
{
	GENERATED_BODY()

public:
	AOCPiecePawn();

	virtual bool MoveTo(AOCBoardTile* BoardTile) override;
	
	virtual TArray<AOCBoardTile*> GetLegalMoves(bool bFilterMoves = true) override;

	virtual TArray<AOCBoardTile*> GetAttackMoves(bool bFilterMoves = true) override;

private:
	bool bIsFirstMove;

	TArray<FOCBoardCoordinates> GetSpecificMoves() const;
};

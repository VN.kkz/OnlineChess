// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "GameFramework/Pawn.h"
#include "OCPlayerPawn.generated.h"

class UCameraComponent;
class UOCEnhancedSpringArmComponent;
class UOCInteractionComponent;
class UInputMappingContext;
struct FInputActionValue;
class UOCInputConfigData;
enum class EPieceColor : uint8;

UENUM()
enum class EChessSoundType : uint8
{
	Move,
	Capture,
	Check,
	Mate,
	Login,
};

UCLASS()
class ONLINECHESS_API AOCPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	AOCPlayerPawn();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	void SetCanMovePieces(bool bCanMove);
	
	void SetupColor(EPieceColor PieceColor);

	UFUNCTION(Client, Reliable)
	void Client_RotateBoardView();
	
	EPieceColor GetColor() const;

protected:
	UPROPERTY(Replicated)
	bool bCanMovePieces;
	
	UPROPERTY(Replicated)
	EPieceColor Color;
	
	UPROPERTY(EditDefaultsOnly, Category = "Input")
	UOCInputConfigData* InputConfigData;

	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
	UOCInteractionComponent* InteractionComponent;

	UPROPERTY()
	USceneComponent* DefaultRootComponent;
	
	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	UOCEnhancedSpringArmComponent* EnhancedSpringArmComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Camera")
	UCameraComponent* CameraComponent;
	
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void InputOpenOptionsMenu();

private:
	FTimerHandle TimerHandle_StartGameUpdate;
	
	float StartGameUpdateRate;
	
	void InputSelectOrPlacePiece();
	
	void InputHoldPiece();
	
	void InputPlacePiece();

	void InputDeselectPiece();
	
	void InputZoomCamera(const FInputActionValue& AxisValue);
	
	void InputLockCameraRotation();
	
	void InputRotateCameraByY(const FInputActionValue& AxisValue);
	
	void InputRotateCameraByZ(const FInputActionValue& AxisValue);

public:
	void PlayCustomSound(EChessSoundType SoundType);

protected:
	UFUNCTION(NetMulticast, Unreliable)
	void Multicast_PlayCustomSound(EChessSoundType SoundType);
	
	UPROPERTY(EditDefaultsOnly)
	USoundBase* MoveSound;
	
	UPROPERTY(EditDefaultsOnly)
	USoundBase* CaptureSound;
	
	UPROPERTY(EditDefaultsOnly)
	USoundBase* CheckSound;
	
	UPROPERTY(EditDefaultsOnly)
	USoundBase* MateSound;
	
	UPROPERTY(EditDefaultsOnly)
	USoundBase* LoginSound;
};

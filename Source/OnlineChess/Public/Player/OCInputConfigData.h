// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "OCInputConfigData.generated.h"

class UInputMappingContext;
class UInputAction;

/**
 * 
 */
UCLASS()
class ONLINECHESS_API UOCInputConfigData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category = "Input")
	UInputMappingContext* InputMappingContext; 
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InputAction")
	UInputAction* SelectPiece;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InputAction")
	UInputAction* DeselectPiece;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InputAction")
	UInputAction* ZoomCamera;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InputAction")
	UInputAction* RotateCamera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InputAction")
	UInputAction* RotateCameraMouseDragY;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InputAction")
	UInputAction* RotateCameraMouseDragZ;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InputAction")
	UInputAction* OpenOptionsMenu;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "OCPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ONLINECHESS_API AOCPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AOCPlayerController();

	void FlipFlopMouseVisibility();

private:
	FVector2d SavedMouseLocation;
};

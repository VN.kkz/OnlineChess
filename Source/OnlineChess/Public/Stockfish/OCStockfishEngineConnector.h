﻿#pragma once

#include <windows.h>
#include <stdio.h>
#include <iostream>

class OCStockfishEngineConnector
{
public:
	OCStockfishEngineConnector();
	
	~OCStockfishEngineConnector();
	
	void ConnectWithPath(FString Path);

	FString GetBestMove(FString Moves, int32 Depth);

private:
	STARTUPINFOA StartupInfo;
	
	SECURITY_ATTRIBUTES SecurityAttributes;
	
	PROCESS_INFORMATION ProcessInformation;
	
	FString ExecutableFilename;
	
	HANDLE HandleInWrite;
	
	HANDLE HandleInRead;
	
	HANDLE HandleOutWrite;
	
	HANDLE HandleOutRead;
	
	DWORD Write;
	
	DWORD Read;
	
	DWORD Available;
	
	BYTE Buffer[2048];

	int32 SleepDelay;

	bool bHasBeenConnected;
};
